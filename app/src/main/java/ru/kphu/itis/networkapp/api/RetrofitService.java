package ru.kphu.itis.networkapp.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Дмитрий on 30.10.2017.
 */

public interface RetrofitService {

    @GET("/get")
    Call<ResponseBody> getData();

    @POST("post")
    Call<ResponseBody> postData();

}
