package ru.kphu.itis.networkapp.api;

import android.support.annotation.NonNull;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Дмитрий on 30.10.2017.
 */

public class InputStreamService {

    @NonNull
    public String load(@NonNull String urlStr) throws IOException {
        URL url = new URL(urlStr);
        InputStream in = url.openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }
}
