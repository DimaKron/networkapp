package ru.kphu.itis.networkapp.view;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import ru.kphu.itis.networkapp.AppDelegate;
import ru.kphu.itis.networkapp.Const;
import ru.kphu.itis.networkapp.R;
import ru.kphu.itis.networkapp.loader.InputStreamLoader;
import ru.kphu.itis.networkapp.loader.OkHttpLoader;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks, Callback<ResponseBody> {

    private static final int LOADER_NETWORK = 111;

    @BindView(R.id.text_view_result) TextView resultTextView;

    @BindView(R.id.button_start) Button startButton;

    @BindView(R.id.button_start_retrofit) Button startRetrofitButton;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(unbinder!=null){
            unbinder.unbind();
        }
    }

    @OnClick(R.id.button_start)
    public void onStartClick(){
        getLoaderManager().initLoader(LOADER_NETWORK, null, this);
    }

    @OnClick(R.id.button_start_retrofit)
    public void onStartRetrofitClick(){
        // TODO GOTO LOADER with execute()
        ((AppDelegate)getApplication()).getRetrofit().getData().enqueue(this);
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        switch (i){
            case LOADER_NETWORK:
                return new OkHttpLoader(this, Const.URL_GET);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader loader, Object result) {
        switch (loader.getId()){
            case LOADER_NETWORK:
                onNetworkLoadingSuccess((String) result);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {}

    private void onNetworkLoadingSuccess(@Nullable String result){
        startButton.setVisibility(View.GONE);
        startRetrofitButton.setVisibility(View.GONE);
        resultTextView.setText(result);
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        try {
            onNetworkLoadingSuccess(response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable throwable) {}
}
