package ru.kphu.itis.networkapp.loader;

import android.content.AsyncTaskLoader;
import android.content.Context;


public abstract class BaseAsyncTaskLoader<R> extends AsyncTaskLoader{

    public BaseAsyncTaskLoader(Context context) {
        super(context);
    }

    protected abstract R onLoad();

    @Override
    public R loadInBackground() {
        return onLoad();
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
