package ru.kphu.itis.networkapp.loader;

import android.content.Context;
import android.util.Log;

import java.io.IOException;

import ru.kphu.itis.networkapp.api.InputStreamService;
import ru.kphu.itis.networkapp.api.OkHttpService;

/**
 * Created by Дмитрий on 30.10.2017.
 */

public class OkHttpLoader extends BaseAsyncTaskLoader<String> {

    private static final String LOG_TAG = "OkHttpLoader";

    private String url;

    public OkHttpLoader(Context context, String url) {
        super(context);
        this.url = url;
    }

    @Override
    protected String onLoad() {
        try {
            return new OkHttpService().load(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, e.getMessage());
        }
        return null;
    }
}
